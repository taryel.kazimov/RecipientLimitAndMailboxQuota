﻿<# This form was created using POSHGUI.com  a free online gui designer for PowerShell
.NAME
    Untitled
#>
Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn
Add-Type -AssemblyName System.Windows.Forms
[System.Windows.Forms.Application]::EnableVisualStyles()

#region begin GUI{ 

$RecipientManagement             = New-Object system.Windows.Forms.Form
$RecipientManagement.ClientSize  = '861,402'
$RecipientManagement.text        = "Exchange Tool"
$RecipientManagement.BackColor   = "#9b9b9b"
$RecipientManagement.TopMost     = $false

$Mailbox                         = New-Object system.Windows.Forms.TextBox
$Mailbox.multiline               = $false
$Mailbox.text                    = ""
$Mailbox.BackColor               = "#b8e986"
$Mailbox.width                   = 175
$Mailbox.height                  = 20
$Mailbox.location                = New-Object System.Drawing.Point(14,18)
$Mailbox.Font                    = 'Palatino Linotype,12'

$ListBox1                        = New-Object system.Windows.Forms.ListBox
$ListBox1.text                   = "listBox"
$ListBox1.width                  = 175
$ListBox1.height                 = 258
$ListBox1.location               = New-Object System.Drawing.Point(14,50)

$Button2                         = New-Object system.Windows.Forms.Button
$Button2.BackColor               = "#4a4a4a"
$Button2.text                    = "Recipient Limiti Yoxla"
$Button2.width                   = 180
$Button2.height                  = 31
$Button2.location                = New-Object System.Drawing.Point(207,68)
$Button2.Font                    = 'Palatino Linotype,12'
$Button2.ForeColor               = 'White'

$Button3                         = New-Object system.Windows.Forms.Button
$Button3.BackColor               = "#4a4a4a"
$Button3.text                    = "Mailbox Həcmi Yoxla"
$Button3.width                   = 180
$Button3.height                  = 31
$Button3.location                = New-Object System.Drawing.Point(207,119)
$Button3.Font                    = 'Palatino Linotype,12'
$Button3.ForeColor               = 'White'

$ComboBox1                       = New-Object system.Windows.Forms.ComboBox
$ComboBox1.text                  = "box"
$ComboBox1.width                 = 100
$ComboBox1.height                = 20
$ComboBox1.location              = New-Object System.Drawing.Point(404,68)
$ComboBox1.Font                  = 'Palatino Linotype,12'

$ComboBox2                       = New-Object system.Windows.Forms.ComboBox
$ComboBox2.text                  = "comboBox"
$ComboBox2.width                 = 100
$ComboBox2.height                = 20
$ComboBox2.location              = New-Object System.Drawing.Point(404,119)
$ComboBox2.Font                  = 'Palatino Linotype,12'

$TextBox1                        = New-Object system.Windows.Forms.TextBox
$TextBox1.multiline              = $false
$TextBox1.BackColor              = "#b8e986"
$TextBox1.width                  = 100
$TextBox1.height                 = 20
$TextBox1.location               = New-Object System.Drawing.Point(577,68)
$TextBox1.Font                   = 'Palatino Linotype,12'

$TextBox2                        = New-Object system.Windows.Forms.TextBox
$TextBox2.multiline              = $false
$TextBox2.BackColor              = "#b8e986"
$TextBox2.width                  = 100
$TextBox2.height                 = 20
$TextBox2.location               = New-Object System.Drawing.Point(577,119)
$TextBox2.Font                   = 'Palatino Linotype,12'

$CheckBox1                       = New-Object system.Windows.Forms.CheckBox
$CheckBox1.AutoSize              = $false
$CheckBox1.width                 = 20
$CheckBox1.height                = 20
$CheckBox1.location              = New-Object System.Drawing.Point(547,68)
$CheckBox1.Font                  = 'Palatino Linotype,12'


$CheckBox2                       = New-Object system.Windows.Forms.CheckBox
$CheckBox2.AutoSize              = $false
$CheckBox2.width                 = 20
$CheckBox2.height                = 20
$CheckBox2.location              = New-Object System.Drawing.Point(547,119)
$CheckBox2.Font                  = 'Palatino Linotype,12'

$Button1                         = New-Object system.Windows.Forms.Button
$Button1.BackColor               = "#4a4a4a"
$Button1.text                    = "Axtar"
$Button1.width                   = 180
$Button1.height                  = 31
$Button1.location                = New-Object System.Drawing.Point(206,17)
$Button1.Font                    = 'Palatino Linotype,12'
$Button1.ForeColor               = "#ffffff"

$Button4                         = New-Object system.Windows.Forms.Button
$Button4.BackColor               = "#4a4a4a"
$Button4.text                    = "Set"
$Button4.width                   = 100
$Button4.height                  = 30
$Button4.location                = New-Object System.Drawing.Point(691,68)
$Button4.Font                    = 'Palatino Linotype,12'
$Button4.ForeColor               = "#ffffff"

$Button5                         = New-Object system.Windows.Forms.Button
$Button5.BackColor               = "#4a4a4a"
$Button5.text                    = "Set"
$Button5.width                   = 100
$Button5.height                  = 30
$Button5.location                = New-Object System.Drawing.Point(691,119)
$Button5.Font                    = 'Palatino Linotype,12'
$Button5.ForeColor               = "#ffffff"

$RecipientManagement.controls.AddRange(@($Mailbox,$ListBox1,$Button2,$Button3,$ComboBox1,$ComboBox2,$TextBox1,$TextBox2,$CheckBox1,$CheckBox2,$Button1,$Button4,$Button5))

# TextBox1 onceden disable et.
$TextBox1.Enabled = $false

# Eger CheckBox1 secilerse 1ci combobox disable olur textbox1 enable olur
$checkbox1.Add_CheckStateChanged({
if ($checkBox1.Checked){ 
$ComboBox1.Enabled = $false
$TextBox1.Enabled = $true }
else { $ComboBox1.Enabled = $true
$TextBox1.Enabled = $false }
})

# TextBox2 onceden disable et.
$TextBox2.Enabled = $false

# Eger CheckBox2 secilerse 1ci combobox disable olur textbox2 enable olur
$checkbox2.Add_CheckStateChanged({
if ($checkBox2.Checked){ 
$ComboBox2.Enabled = $false
$TextBox2.Enabled = $true }
else { $ComboBox2.Enabled = $true
$TextBox2.Enabled = $false }
})

# Axtaris xanasi bos oldugda Axtar duyemsi calismasin.

$Button1.Enabled = $false
$Mailbox.add_TextChanged({IsThereText})
function IsThereText
{
	if (($Mailbox.Text.Length -ne 0))
	{
		$Button1.Enabled = $true

	}
	else
	{
		$Button1.Enabled = $false

    }
}

#ComboBox1

$RecLimits = @(20,30,40,50,60,70,80,90,100)
foreach($limit in $RecLimits)
{
  $comboBox1.Items.add($limit)
}
$ComboBox1.SelectedIndex = 1
$RecipientManagement.Controls.Add($comboBox1)

#ComboBox2

$quotalimit = ("512","1024","1532","2048","2560","3072","3584","4096","4608","5120","5632","6144")
foreach($qlimit in $quotalimit)
{
  $comboBox2.Items.add($qlimit)
}
$ComboBox2.SelectedIndex = 1
$RecipientManagement.Controls.Add($comboBox2)

#region gui events {

$Button1.Add_Click({
$input = $Mailbox.Text
$result = ((get-mailbox -Identity "*$input*" -ErrorAction SilentlyContinue).PrimarySmtpAddress) -join "  "
$res = $result.split("",[System.StringSplitOptions]::RemoveEmptyEntries) 
$ListBox1.Items.Clear()
foreach ($mailbox in $res)
{
[Void]$ListBox1.Items.Add($mailbox)
}

})


$Button2.Add_Click({
$RecipientLimits = (Get-Mailbox -Identity $ListBox1.SelectedItem).RecipientLimits
[System.Windows.MessageBox]::Show("Recipient limit is: $RecipientLimits")
  })


$Button3.Add_Click({
$quota = (Get-Mailbox -Identity $ListBox1.SelectedItem).ProhibitSendQuota
[System.Windows.MessageBox]::Show("Mailbox quota is: $quota")
  })


$Button4.Add_Click({  })
$Button5.Add_Click({  })
#endregion events }

#endregion GUI }




[void]$RecipientManagement.ShowDialog()